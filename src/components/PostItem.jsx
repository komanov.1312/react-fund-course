import React from 'react';
import { useHistory } from 'react-router';
import MyButton from './UI/button/MyButton';

const PostItem = ({ post, remove, post: { title, body, id } }) => {
  const router = useHistory();
  return (
    <div className='post'>
      <div className='post__content'>
        <strong>
          {id}. {title}
        </strong>
        <div>{body}</div>
      </div>
      <div className='post__btns'>
        <MyButton onClick={() => router.push(`/posts/${id}`)}>Открыть</MyButton>
        <MyButton onClick={() => remove(post)}>Удалить</MyButton>
      </div>
    </div>
  );
};

export default PostItem;
