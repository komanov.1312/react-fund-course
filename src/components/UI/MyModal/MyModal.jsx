import React from 'react';
import cl from './MyModal.module.css';
import ReactDOM from 'react-dom';

// eslint-disable-next-line no-unused-vars
const MyModal = ({ children, visible, setVisible }) => {
  const rootClasses = [cl.myModal];
  if (visible) {
    rootClasses.push(cl.active);
  }
  return ReactDOM.createPortal(
    <div className={rootClasses.join(' ')} onClick={() => setVisible(false)}>
      <div className={cl.myModalContent} onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>,
    document.body
  );
};

export default MyModal;
